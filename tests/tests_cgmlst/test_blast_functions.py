import pytest

from coreprofiler.cgmlst.blast_functions import (
    BlastConfig,
    create_blast_db,
    run_blast,
)

from coreprofiler.cgmlst.constants import BLASTDB_EXTENSIONS

from pathlib import Path

###########################
# --- run_blast tests --- #
###########################


@pytest.fixture
def blastn_config(kp_db_sample):
    return BlastConfig(
        blast_type="blastn",
        query_path=Path("tests/data/genomes/10_04A025_hennart2022.fas"),
        task="blastn",
        num_threads=4,
        perc_identity=90,
        max_target_seqs=10,
        word_size=31,
        ungapped=False,
        outfmt=(
            "6",
            "evalue",
            "bitscore",
            "pident",
            "sseqid",
            "slen",
            "length",
            "nident",
            "qseqid",
            "qstart",
            "qend",
            "sstrand",
        ),
        db_path=kp_db_sample,
        subject=None,
    )


def test_run_blastn(blastn_config):
    blast_output_df = run_blast(blastn_config)
    assert len(blast_output_df) == 3


def test_run_megablast(blastn_config):
    blastn_config.task = "megablast"
    blast_output_df = run_blast(blastn_config)
    assert len(blast_output_df) == 3


@pytest.fixture
def tblastn_config():
    return BlastConfig(
        blast_type="tblastn",
        query_path="tests/data/protein_samples.fasta",
        num_threads=4,
        max_target_seqs=1,
        ungapped=False,
        evalue="1E-20",
        outfmt="6 evalue bitscore pident sseqid slen length \
            nident qseqid qstart qend",
        subject="tests/data/genomes/10_04A025_hennart2022.fas",
    )


@pytest.mark.skip(reason="CDS option under development.")
def test_run_tblastn(tblastn_config):
    tblastn_output_df = run_blast(tblastn_config)
    assert len(tblastn_output_df) == 3


#################################
# --- create_blast_db tests --- #
#################################


@pytest.fixture
def create_blast_db_valid_inputs(tmp_path):
    loci_names = ["accB_S", "accC_S", "accD_S"]
    scheme_path = Path("tests/data/kp_scheme_sample/")
    blast_db_name = "test_kp_db"
    blast_db_path = tmp_path
    return loci_names, scheme_path, blast_db_name, blast_db_path


def test_create_blast_db_valid(create_blast_db_valid_inputs):
    (
        loci_names,
        scheme_path,
        blast_db_name,
        blast_db_path,
    ) = create_blast_db_valid_inputs
    create_blast_db(
        scheme_path,
        blast_db_name,
        blast_db_path,
        False,
        loci_names,
    )
    assert all(
        (blast_db_path / blast_db_name).with_suffix(".fasta" + ext).exists()
        for ext in BLASTDB_EXTENSIONS
    )
    