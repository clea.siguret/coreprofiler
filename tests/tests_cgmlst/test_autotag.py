from pathlib import Path
from coreprofiler.cgmlst import autotag


query_file = Path("tests/data/genomes/10_04A025_hennart2022.fas")
num_threads = 4


def test_autotag(kp_db_sample):
    expected_matches = {
        "accB_S": "1",
    }
    matches = autotag.autotag(
        query=query_file,
        blast_db_path=kp_db_sample,
        autotag_word_size=31,
        num_threads=num_threads,
        verbose=False,
    )
    assert matches == expected_matches
