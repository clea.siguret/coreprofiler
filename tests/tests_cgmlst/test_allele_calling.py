import hashlib
import pickle
import pytest
from argparse import Namespace
from Bio.Seq import Seq
from pathlib import Path

from coreprofiler.cgmlst import allele_calling


@pytest.fixture(scope="module")
def args():
    args = Namespace(
        query=[Path("tests/data/genomes/10_04A025_hennart2022.fas")],
        scheme_directory=Path("tests/data/kp_scheme_sample"),
        verbose=False,
        blast_db_path=Path(
            "tests/data/kp_scheme_sample_db/kp_scheme_sample_db.fasta"
        ),
        num_threads=4,
        autotag_word_size=31,
        cds=False,
        detailed=False,
        min_id_new_allele=90,
        min_cov_new_allele=70,
        min_cov_incomplete=70,
        profiles_w_tmp_alleles="",
        hash=False,
        num_alleles_per_locus="",
    )
    return args


######################
# --- user runs ---- #
######################


def test_allele_calling_minimal_run(args, tmp_path):
    """
    Minimal run, no database provided, only user profiles put out
    """
    args.out = tmp_path / "test_output.tsv"
    args.blast_db_path = ""

    allele_calling.allele_calling(args)
    assert args.out.exists()

    with open(args.out, "r") as file:
        content = file.read()
        expected_data = (
            "seq_id	accB_S\taccC_S\taccD_S\n"
            + "tests/data/genomes/10_04A025_hennart2022.fas\t1\t2\t2\n"
        )
        assert content == expected_data


def test_allele_calling_minimal_with_db(args, tmp_path):
    """
    Database provided, only user profiles put out
    """
    args.out = tmp_path / "test_output.tsv"

    allele_calling.allele_calling(args)
    assert args.out.exists()

    with open(args.out, "r") as file:
        content = file.read()
        expected_data = (
            "seq_id	accB_S\taccC_S\taccD_S\n"
            + "tests/data/genomes/10_04A025_hennart2022.fas\t1\t2\t2\n"
        )
        assert content == expected_data


##########################
# --- platform runs ---- #
##########################


def test_allele_calling_platform_output(args, tmp_path):
    """
    Database provided, put out profile_w_tmp_alleles.pickle
    """
    args.out = tmp_path / "test_output.tsv"
    args.profiles_w_tmp_alleles = (
        tmp_path / "profiles_w_tmp_alleles_file.pickle"
    )

    allele_calling.allele_calling(args)

    assert args.out.exists()
    with open(args.out, "r") as file:
        content = file.read()
    expected_data = (
        "seq_id	accB_S\taccC_S\taccD_S\n"
        + "tests/data/genomes/10_04A025_hennart2022.fas\t1\t2\t2\n"
    )
    assert content == expected_data

    assert args.profiles_w_tmp_alleles.exists()
    with open(args.profiles_w_tmp_alleles, "rb") as f:
        output_dict = pickle.load(f)
    assert output_dict == {
        "tests/data/genomes/10_04A025_hennart2022.fas": {
            "genome": "tests/data/genomes/10_04A025_hennart2022.fas",
            "profile": str(args.out),
            "tmp_loci": ["accC_S", "accD_S"],
        }
    }


def test_allele_calling_platform_hash(args, tmp_path):
    """
    Verify that MD5-hash are correct
    """
    args.out = tmp_path / "test_output.tsv"
    args.hash = True

    with open(Path("tests/data/test_scannew/kp_accC_S_20.tfa"), "r") as f:
        f.readline()
        accc_s_20 = f.readline().strip()
        hash_acc_s_20 = (
            "accC_S_" + hashlib.md5(str(accc_s_20).encode()).hexdigest()
        )

    with open(Path("tests/data/test_scannew/kp_accD_S_37.tfa"), "r") as f:
        f.readline()
        # minus sense
        accd_s_37 = str(Seq(f.readline().strip()).reverse_complement())
        hash_acc_d_37 = (
            "accD_S_" + hashlib.md5(str(accd_s_37).encode()).hexdigest()
        )

    allele_calling.allele_calling(args)

    assert args.out.exists()
    with open(args.out, "r") as file:
        content = file.read()
    expected_data = (
        "seq_id	accB_S\taccC_S\taccD_S\n"
        + "tests/data/genomes/10_04A025_hennart2022.fas\t"
        + f"1\t{hash_acc_s_20}\t{hash_acc_d_37}\n"
    )
    assert content == expected_data


def test_allele_calling_platform_num_alleles(args, tmp_path):
    """
    Test num_alleles_per_locus
    """
    args.out = tmp_path / "test_output.tsv"
    args.num_alleles_per_locus = Path(
        "tests/data/num_alleles_per_locus/kp.tsv"
    )
    args.hash = False

    allele_calling.allele_calling(args)

    assert args.out.exists()
    with open(args.out, "r") as file:
        content = file.read()
    expected_data = (
        "seq_id	accB_S\taccC_S\taccD_S\n"
        + "tests/data/genomes/10_04A025_hennart2022.fas\t"
        + "1\t19\t113\n"
    )
    assert content == expected_data
