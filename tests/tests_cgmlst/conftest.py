from pathlib import Path
from tempfile import TemporaryDirectory
import pytest

from coreprofiler.cgmlst import blast_functions


@pytest.fixture(scope="package")
def kp_db_sample():
    """BLASTdb from data/kp_scheme_sample_db/"""
    scheme_path = Path("tests/data/kp_scheme_sample/")
    with TemporaryDirectory() as db_dir:
        db_path = Path(db_dir)
        blast_functions.create_blast_db(
            scheme_path=scheme_path,
            blast_db_name="kp_db_sample",
            blast_db_path=db_path,
            verbose=True,
        )
        yield db_path / "kp_db_sample.fasta"
