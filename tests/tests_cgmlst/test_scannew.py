import hashlib
import pytest

from Bio.Seq import Seq
from pathlib import Path

from coreprofiler.cgmlst.scannew import scannew


def test_scannew_kp():
    """Test that alleles detected considering only a sample of the database
    match the real-life alleles.
    """
    query_file = Path("tests/data/genomes/10_04A025_hennart2022.fas")
    no_exact_match_loci = ["accC_S", "accD_S"]
    scheme_directory = Path("tests/data/kp_scheme_sample")

    with open(Path("tests/data/test_scannew/kp_accC_S_20.tfa"), "r") as f:
        f.readline()
        accc_s_20 = f.readline().strip()

    with open(Path("tests/data/test_scannew/kp_accD_S_37.tfa"), "r") as f:
        f.readline()
        accd_s_37 = str(Seq(f.readline().strip()).reverse_complement())

    new_alleles_df, scannew_dict = scannew(
        query_file,
        no_exact_match_loci,
        scheme_directory,
        num_threads=4,
        cds=False,
        verbose=False,
        detailed=False,
        min_id=90,
        min_cov_new_allele=90,
        min_cov_incomplete=70,
    )
    expected_scannew_dict = {
        # plus sense
        "accC_S": "accC_S_"
        + hashlib.md5(str(accc_s_20).encode()).hexdigest(),
        # minus sense
        "accD_S": "accD_S_"
        + hashlib.md5(str(accd_s_37).encode()).hexdigest(),
    }
    assert scannew_dict == expected_scannew_dict

    assert (
        new_alleles_df.loc[new_alleles_df["locus"] == "accC_S", "seq"][0]
        == accc_s_20
    )
    assert (
        new_alleles_df.loc[new_alleles_df["locus"] == "accD_S", "seq"][1]
        == accd_s_37
    )
