import pytest

from coreprofiler.__main__ import main


def test_main_no_args(capsys, monkeypatch):
    """Print help and exit if no arg provided."""
    args = ["coreprofiler"]
    monkeypatch.setattr("sys.argv", args)

    with pytest.raises(SystemExit) as excinfo:
        main()

    captured = capsys.readouterr()
    assert "cgMLST tool based on BLAST." in captured.err
    assert excinfo.value.code == 1
