#!/bin/bash
set -e

coreprofiler allele_calling \
   -q tests/data/genomes/10_04A025_hennart2022.fas \
   -sf tests/data/kp_scheme_sample \
   -db tests/data/kp_scheme_sample_db/kp_scheme_sample_db.fasta \
   -out tests/tests_cgmlst/test_output_allele_calling.tsv