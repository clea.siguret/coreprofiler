import json
import pytest

from datetime import datetime
from pathlib import Path
from coreprofiler.db.api import (
    download_scheme,
    fetch_remote_alleles,
    update_scheme,
)

######################################
# --- fetch_remote_alleles tests --- #
######################################

# On BIGSdb

base_url_pasteur = "https://bigsdb.pasteur.fr/api/db/"
base_url_oxford = "https://rest.pubmlst.org/db/"


@pytest.mark.parametrize(
    "species, locus_url",
    [
        (
            "K. pneumoniae",
            f"{base_url_pasteur}pubmlst_klebsiella_seqdef/loci/rbsC_S",
        ),
        (
            "S. epidermidis",
            f"{base_url_pasteur}pubmlst_epidermidis_seqdef/loci/SE2419",
        ),
        (
            "S. aureus",
            f"{base_url_oxford}pubmlst_saureus_seqdef/loci/SAUR0001",
        ),
        (
            "S. peumoniae",
            f"{base_url_oxford}pubmlst_spneumoniae_seqdef/loci/SPNE00001",
        ),
        (
            "A. baumannii",
            f"{base_url_oxford}pubmlst_abaumannii_seqdef/loci/ACIN00005",
        ),
    ],
)
@pytest.mark.api
def test_fetch_remote_alleles_bigsdb(species, locus_url):
    resp = fetch_remote_alleles(locus_url)
    assert isinstance(resp, dict)
    assert set(resp.keys()) == set(["seq", "last_update"])

@pytest.mark.api
def test_fetch_remote_alleles_fail():
    resp = fetch_remote_alleles(f"{base_url_pasteur}/invalid_locus_url")
    assert "Failed to fetch data from url" in resp


################################
# --- download_scheme tests ---#
################################

scheme_api_file = Path("data/scheme_api.json")

@pytest.mark.api
def test_download_scheme_kp(tmp_path):
    scheme_local_path = tmp_path / "kp_scheme"
    scheme_local_path.mkdir(parents=True, exist_ok=True)
    download_scheme(
        scheme_api_file,
        "Klebsiella pneumoniae",
        scheme_local_path,
    )

    files = list(scheme_local_path.iterdir())
    assert len(files) == 629

    # Non-exhaustive list
    expected_files = [
        "KP1_0122_S.fasta",
        "rhaB_S.fasta",
        "rbsC_S.fasta",
        "cpxA_S.fasta",
        "KP1_0077_S.fasta",
    ]
    for filename in expected_files:
        assert (scheme_local_path / filename).is_file()

    for file in files:
        assert file.stat().st_size > 0

@pytest.mark.api
def test_download_scheme_av(capfd):
    download_scheme(scheme_api_file=scheme_api_file, species="av")
    out, err = capfd.readouterr()
    assert "Klebsiella pneumoniae" in out

@pytest.mark.api
def test_download_scheme_invalid_species(capfd):
    download_scheme(scheme_api_file=scheme_api_file, species="invalid_species")
    out, err = capfd.readouterr()
    assert "Invalid species selection. Pleaes choose one species" in out
    assert "Klebsiella pneumoniae" in out


##############################
# --- update_scheme tests ---#
##############################

@pytest.mark.api
def test_update_scheme(tmp_path):
    scheme_local_path = tmp_path / "scheme_local"
    scheme_local_path.mkdir()
    loci = {"rbsC_S": "AAA", "rbsB_S": "AAC", "rbsK_S": "AAG"}
    for locus, seq in loci.items():
        with open(scheme_local_path / f"{locus}.fasta", "w+") as lf:
            lf.write(seq)

    mock_api_file = tmp_path / "mock_api_file.json"
    kp_url = (
        "https://bigsdb.pasteur.fr/api/db/pubmlst_klebsiella_seqdef/schemes/18"
    )
    api_info = {
        "Klebsiella pneumoniae": {
            "description": "mock_scheme",
            "url": kp_url,
            "last_local_update": "1871-03-18",
        }
    }
    with open(mock_api_file, "w+") as maf:
        maf.write(json.dumps(api_info, indent=4))

    update_scheme(
        species="Klebsiella pneumoniae",
        scheme_file=mock_api_file,
        scheme_local_path=scheme_local_path,
        log_dir_path=tmp_path / "logs",
    )

    # Verify that files contain many sequences
    for locus in loci.keys():
        file = scheme_local_path / f"{locus}.fasta"
        assert file.stat().st_size > 5 * 10**5

    # Verify the log file
    today = datetime.today().strftime("%Y-%m-%d")
    expected_dict = {today: ["rbsK_S", "rbsB_S", "rbsC_S"]}
    with open(tmp_path / f"logs/{today}.log") as logf:
        log_dict = json.load(logf)
        
        assert sorted(log_dict) == sorted(expected_dict)
