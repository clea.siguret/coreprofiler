import json
import pickle
import pytest
import shutil

import pandas as pd

from pathlib import Path

from coreprofiler.__main__ import main

from coreprofiler.platform.platform_utils import (
    ProfileConflictError,
    update_profile,
    update_profiles_from_logs,
)


@pytest.fixture(scope="module")
def tmp_dir_module(tmp_path_factory):
    yield tmp_path_factory.mktemp("update_profile")


@pytest.fixture(scope="module")
def new_scheme_files(tmp_dir_module):
    """Add accC_S_20 allele (exact match on 10_04A025_hennart2022)
    to the scheme files.
    """
    # Create the new scheme files
    scheme_dir_up = tmp_dir_module / "updated_scheme_files"
    shutil.copytree("tests/data/kp_scheme_sample", scheme_dir_up)
    # add the allele that matches exactly
    with open(scheme_dir_up / "accC_S.tfa", "a") as f1, open(
        "tests/data/test_scannew/kp_accC_S_20.tfa"
    ) as f2:
        f1.write(f2.read())

    return scheme_dir_up


################################
# --- update_profile tests --- #
################################


def test_update_profile_new_exact_match(tmp_dir_module, new_scheme_files):
    """Test that the temporary allele id for locus accC_S in
    10_04A025_hennart2022's profile is replaced by "20".
    """
    # tmp_path, scheme_dir_up = new_scheme_files
    # Copy the profile file (the function will replace the input file)
    profile_up = tmp_dir_module / "10_04A025_hennart2022_hash_up_accC_S.tsv"

    shutil.copyfile(
        "tests/data/profiles/10_04A025_hennart2022_hash.tsv",
        profile_up,
    )
    with open(profile_up, "r") as puf:
        profile_former = pd.read_csv(puf, sep="\t", index_col=0)

    loci_exact_matches = update_profile(
        genome_path=Path("tests/data/genomes/10_04A025_hennart2022.fas"),
        profile_path=profile_up,
        loci_updated=["accC_S"],
        scheme_files_dir=new_scheme_files,
    )

    with open(profile_up, "r") as pun:
        profile_new = pd.read_csv(pun, sep="\t", index_col=0)

    profile_expected = profile_former
    profile_expected.loc[
        "tests/data/genomes/10_04A025_hennart2022.fas", "accC_S"
    ] = "20"
    profile_expected["accC_S"] = profile_expected["accC_S"].astype("int64")

    pd.testing.assert_frame_equal(profile_new, profile_expected)
    assert loci_exact_matches == ["accC_S"]


def test_update_profile_no_value(tmp_dir_module, new_scheme_files):
    """Test that there is no change if run again on locus accD_S
    (no allele has been added -> still temporary allele).
    """
    profile_up = tmp_dir_module / "10_04A025_hennart2022_hash_up_accD_S.tsv"
    shutil.copyfile(
        "tests/data/profiles/10_04A025_hennart2022_hash.tsv",
        profile_up,
    )
    with open(profile_up, "r") as puf:
        profile_former = pd.read_csv(puf, sep="\t", index_col=0)

    loci_exact_matches = update_profile(
        genome_path=Path("tests/data/genomes/10_04A025_hennart2022.fas"),
        profile_path=profile_up,
        loci_updated=["accD_S"],
        scheme_files_dir=new_scheme_files,
    )

    with open(profile_up, "r") as pun:
        profile_new = pd.read_csv(pun, sep="\t", index_col=0)

    pd.testing.assert_frame_equal(profile_new, profile_former)
    assert not loci_exact_matches


@pytest.mark.exception
def test_update_profile_error(tmp_dir_module, new_scheme_files):
    """Raise an exception if trying to redefine a value that is
    an integer (i.e., not a temporary nor missing allele).
    """
    profile_up = tmp_dir_module / "10_04A025_hennart2022_hash_up_accB_S.tsv"
    shutil.copyfile(
        "tests/data/profiles/10_04A025_hennart2022_hash.tsv",
        profile_up,
    )

    with pytest.raises(ProfileConflictError):
        update_profile(
            genome_path=Path("tests/data/genomes/10_04A025_hennart2022.fas"),
            profile_path=profile_up,
            loci_updated=["accB_S"],
            scheme_files_dir=new_scheme_files,
        )


###########################################
# --- update_profiles_from_logs tests --- #
###########################################


@pytest.fixture(scope="module")
def tmp_dir_module_2(tmp_path_factory):
    yield tmp_path_factory.mktemp("update_profiles_from_logs")


@pytest.fixture
def mock_update_log(tmp_dir_module_2):
    mock_log_path = tmp_dir_module_2 / "mock_log_file.json"
    with open(mock_log_path, "w+") as mlf:
        json.dump(
            {"date": "1871-03-18", "loci_updated": ["accC_S"]}, mlf, indent=4
        )
    return mock_log_path


@pytest.fixture(scope="function")
def mock_profile(tmp_path):
    # Copy the profile file (the function will replace the input file)
    profile = tmp_path / "10_04A025_hennart2022_hash_up_accC_S_2.tsv"
    shutil.copyfile(
        "tests/data/profiles/10_04A025_hennart2022_hash.tsv",
        profile,
    )
    return profile


@pytest.fixture(scope="function")
def mock_profiles_w_temp_alleles_file(tmp_dir_module_2, mock_profile):
    genome_path = Path("tests/data/genomes/10_04A025_hennart2022.fas")

    mock_profiles_w_temp_alleles_file = (
        tmp_dir_module_2 / "profiles_w_temp_alleles_file.pickle"
    )
    mock_profiles_w_temp_alleles_dict = {
        "10_04A025_hennart2022": {
            "genome": genome_path,
            "profile": mock_profile,
            "tmp_loci": ["accC_S"],
        }
    }

    with open(mock_profiles_w_temp_alleles_file, "wb") as mp:
        pickle.dump(mock_profiles_w_temp_alleles_dict, mp)

    return mock_profiles_w_temp_alleles_file


def test_update_profiles_from_logs_update(
    new_scheme_files,
    mock_update_log,
    mock_profiles_w_temp_alleles_file,
    mock_profile,
):
    """
    accC_S_20 (exact matches on 10_04A025_hennart2022) is added to the
    database. Log files tell that accC_S has been updated, and that
    10_04A025_hennart2022's profile has a temporary all at the locus accC_S

    -> update of the profile and of profiles_w_temp_alleles
    """
    genome_path = Path("tests/data/genomes/10_04A025_hennart2022.fas")

    with open(mock_profile, "r") as pf:
        profile_up_before = pd.read_csv(pf, sep="\t", index_col=0)

    update_profiles_from_logs(
        mock_update_log,
        mock_profiles_w_temp_alleles_file,
        new_scheme_files,
    )

    with open(mock_profiles_w_temp_alleles_file, "rb") as mptaf:
        mock_profiles_w_temp_alleles_up = pickle.load(mptaf)

    # Assert that the genome has been deleted in the file
    # accC_S matched exactly on 10_04A025_hennart2022
    # and not temporary loci remains
    assert not mock_profiles_w_temp_alleles_up

    # Assert that the profile has been updated
    profile_expected = profile_up_before
    profile_expected.loc[str(genome_path), "accC_S"] = 20
    profile_expected["accC_S"] = profile_expected["accC_S"].astype("int64")

    with open(mock_profile, "r") as pf:
        profile_up_after = pd.read_csv(pf, sep="\t", index_col=0)

    pd.testing.assert_frame_equal(profile_up_after, profile_expected)


def test_update_profiles_from_logs_wrong_locus(
    new_scheme_files, mock_update_log, mock_profile, tmp_path
):
    """
    accC_S_20 (exact matches on 10_04A025_hennart2022) is added to the
    database. Log file tells that accC_S has been updated, but accC_S
    is not listed in 10_04A025_hennart2022's temporary alleles

    -> nothing happens
    """
    with open(mock_profile, "r") as pf:
        profile_up_before = pd.read_csv(pf, sep="\t", index_col=0)

    genome_path = Path("tests/data/genomes/10_04A025_hennart2022.fas")

    # Mock profiles with temporary alleles
    mock_profiles_w_temp_alleles_file = (
        tmp_path / "profiles_w_temp_alleles_file.pickle"
    )
    mock_profiles_w_temp_alleles_dict = {
        "10_04A025_hennart2022": {
            "genome": genome_path,
            "profile": new_scheme_files,
            "tmp_loci": ["ANOTHER_LOCUS"],  # difference here
        }
    }

    with open(mock_profiles_w_temp_alleles_file, "wb") as mp:
        pickle.dump(mock_profiles_w_temp_alleles_dict, mp)

    update_profiles_from_logs(
        mock_update_log,
        mock_profiles_w_temp_alleles_file,
        new_scheme_files,
    )

    with open(mock_profiles_w_temp_alleles_file, "rb") as mptaf:
        mock_profiles_w_temp_alleles_up = pickle.load(mptaf)

    # Assert profiles_w_temp_alleles remains unchanged
    assert mock_profiles_w_temp_alleles_up == mock_profiles_w_temp_alleles_dict

    # Assert profile remains unchanged
    with open(mock_profile, "r") as pf:
        profile_up_after = pd.read_csv(pf, sep="\t", index_col=0)
    pd.testing.assert_frame_equal(profile_up_before, profile_up_after)


def test_update_profiles_from_logs_wrong_locus_2(
    new_scheme_files, mock_profiles_w_temp_alleles_file, mock_profile, tmp_path
):
    """
    accC_S_20 (exact matches on 10_04A025_hennart2022) is added to the
    database but it is not written in the update log.

    -> nothing happens
    """
    mock_log_path = tmp_path / "mock_log_file.json"
    with open(mock_log_path, "w+") as mlf:
        json.dump(
            {
                "date": "1871-03-18",
                "loci_updated": ["ANOTHER_LOCUS"],
            },  # difference here
            mlf,
            indent=4,
        )

    with open(mock_profile, "r") as pf:
        profile_up_before = pd.read_csv(pf, sep="\t", index_col=0)

    with open(mock_profiles_w_temp_alleles_file, "rb") as mptafb:
        mock_profiles_w_temp_alleles_before = pickle.load(mptafb)

    update_profiles_from_logs(
        mock_log_path,
        mock_profiles_w_temp_alleles_file,
        new_scheme_files,
    )

    with open(mock_profiles_w_temp_alleles_file, "rb") as mptafu:
        mock_profiles_w_temp_alleles_up = pickle.load(mptafu)

    # Assert profiles_w_temp_alleles remains unchanged
    assert (
        mock_profiles_w_temp_alleles_up == mock_profiles_w_temp_alleles_before
    )

    # Assert profile remains unchanged
    with open(mock_profile, "r") as pf:
        profile_up_after = pd.read_csv(pf, sep="\t", index_col=0)
    pd.testing.assert_frame_equal(profile_up_before, profile_up_after)


########################
# --- main() tests --- #
########################


def test_platform_no_args(capsys, monkeypatch):
    """Print platform help if no further argument provided."""
    args = ["cgMLST", "platform"]
    monkeypatch.setattr("sys.argv", args)

    with pytest.raises(SystemExit) as excinfo:
        main()

    captured = capsys.readouterr()
    assert "Platform specific arguments." in captured.out
    assert excinfo.value.code == 1


def test_update_profiles_valid(
    monkeypatch,
    mock_profiles_w_temp_alleles_file,
    mock_profile,
    mock_update_log,
    new_scheme_files,
):
    args = [
        "cgMLST",
        "platform",
        "update_profiles",
        "--profiles_w_temp_alleles",
        str(mock_profiles_w_temp_alleles_file),
        "--update_log",
        str(mock_update_log),
        "--scheme_dir",
        str(new_scheme_files),
    ]
    monkeypatch.setattr("sys.argv", args)

    genome_path = Path("tests/data/genomes/10_04A025_hennart2022.fas")

    with open(mock_profile, "r") as pf:
        profile_before = pd.read_csv(pf, sep="\t", index_col=0)

    main()

    with open(mock_profile, "r") as pf:
        profile_after = pd.read_csv(pf, sep="\t", index_col=0)

    with open(mock_profiles_w_temp_alleles_file, "rb") as mptaf:
        mock_profiles_w_temp_alleles_up = pickle.load(mptaf)

    # Assert that the genome has been deleted in the file
    # accC_S matched exactly on 10_04A025_hennart2022
    # and not temporary loci remains
    assert not mock_profiles_w_temp_alleles_up

    # Assert that the profile has been updated
    profile_expected = profile_before
    profile_expected.loc[str(genome_path), "accC_S"] = 20
    profile_expected["accC_S"] = profile_expected["accC_S"].astype("int64")

    with open(mock_profile, "r") as pf:
        profile_after = pd.read_csv(pf, sep="\t", index_col=0)

    pd.testing.assert_frame_equal(profile_after, profile_expected)
