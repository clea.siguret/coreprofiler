# CoreProfiler: a robust and integrable cgMLST software

## Introduction

Core genome Multilocus Sequence Typing (cgMLST) is a genotyping method used in microbiology to compare the genetic distances of bacterial strains. It achieves this by comparing the alleles of the core genome in genomes to a reference database of official alleles. The output is a cgMLST profile. CoreProfiler is a software that calls official alleles ("autotag") and detects new ones ("scannew").

## Usage

### Requirements

- conda

### Prepare environment

- Clone the directory with `git clone`
- Change to that location

    ```bash
    $ cd coreprofiler
    ```

- Create conda environment

    ```bash
    $ conda env create --file=environment.yml
    ```

- Activate conda environment

    ```bash
    $ conda activate coreprofiler
    ```

### Install coreprofiler

- Install coreprofiler

    ```bash
    $ pip install .
    ```

- Check the installation

    ```bash
    $ coreprofiler --version
    ```

### Basic usage

The identification of alleles on the genome, often referred to as 'allele calling,' requires an allele database (or 'scheme'). 

#### Scheme downloading

This scheme contains the list of official alleles curated by its reference platform (such as BIGSdb for *Klebsiella pneumoniae* or *Listeria*, Enterobase for *E. coli*, ...). Each locus has its FASTA file containing the list of alleles, with sequence id's `>[locus]_[allele-number]`. The schemes should be downloaded from the reference platforms and should not be manually modified.

#### Allele identification

The main command `coreprofiler allele_calling` consist of several steps:

1. Autotag (exact matches on reference database)

    It runs `BLASTn` with 100% identity, no gaps, and parameters optimized for speed (`-task megblast` and `-dust no`; genome vs. BLAST db). The output is parsed, only results with 100% coverage are kept.

2. Scannew (detection of new alleles)

    It selects the loci for which no exact match was found in step 1. Creates a BLAST database with those loci files, and runs a looser `BLASTn`(`-task blastn`, genome vs. BLAST db). The best hit is kept for each locus and classified according to the matching values:
    - if identity > 90% and coverage > 90%: considered a new allele
    - identity > 90% and coverage between 70% and 90%: allele present but incomplete; “X”
    - identity < 90% or identity < 90%: missing allele; “-”

CoreProfiler takes as input:
- a genome assembly in fasta format
- the scheme directory (see )

Omitting `--detailed` parameter would characterize incomplete alleles as "-" (downstream analyses programs may only accept integers and "-" as allele id's).

#### Running the command

You can test the tool by running:

```bash
$ coreprofiler allele_calling \
    -q tests/data/genomes/10_04A025_hennart2022.fas \
    -sf tests/data/kp_scheme_sample \
    -out test_kp.tsv
```

Doing so will run `makeblastdb` on the scheme and create a BLAST database on which to run the genome against. This process may take a bit longer and does not need to be re-run unless the scheme has been updated. The path to a BLAST database can also be provided with the `-db` argument.

Example: (once the command above has been run and a BLASTdb has been created already) `coreprofiler allele_calling -q tests/data/genomes/10_04A025_hennart2022.fas -sf tests/data/kp_scheme_sample -db tmp_blast_db/tmp_blast_db.fasta -out test_kp.tsv`

Note:
- unlike other cgMLST softwares, CoreProfiler takes as input the scheme files and can not be only provided its BLAST database.
- `-db` argument must be given the path of the main fasta file of the BLAST database.

### Detection of new alleles
Core-genome MLST brings out at almost every genome alleles that do not belong to the reference scheme. The aim of CoreProfiler is not to describe new "official" alleles, as that may enter in conflict with the platforms providing official schemes. It is however able to describe locally (temporary) new alleles. A genome is considered to carry a new allele if, for a given locus, the following conditions are met:
    - no exact match (100% identity & 100% coverage) with the locus alleles
    - one allele of this locus matched on the genome with:
        - identity identity >  "min_id_new_allele" (default: 90)
        - coverage identity >  "min_cov_new_allele" (default: 90)
The new allele sequences is the query subject that matched best with one allele (position trimming).

For standard use, this locus will be given the identifier "[number of alleles of this locus] + 1" (see "Handling temporary alleles" for further details and options).

### Parameters
general:
- `-q, --query` [required]: path to the genome assemblies
- `-sf, --scheme-directory` [required]: path to the directory containing the scheme
- `-o, --out` [required]: name of the output file, in tsv format
- `-db, --blast_db_path`: path to the BLAST database
- `-n, --num_threads`: number of threads to run BLAST functions on. Default: 4.
- `-V, --version`: version
- `-v, --verbose`: verbosity

autotag:
- `--autotag_word_size"`: word size for autotag BLASTn, use "min len(shortest allele) - 1"

scannew:
- `--min_id_new_allele`: minimum identity percentage to consider new alleles. Default: 90.
- `--min_cov_new_allele`: minimum coverage percentage to consider new alleles. Default: 90.
- `--min_cov_incomplete`: minimum coverage percentage to consider an allele incomplete (if `--detailed option`)
- `-d`, `--detailed`: return detailed information on incomplete alleles: "incomplete, ...". Else, only new alleles or missing (default).

## Platform integration
CoreProfiler has several modes: `allele_calling` (see above), `db` (to download & updates databases), and `platform` (handle temporary alleles).

### Summary
Scheme databases can be downloaded with `coreprofiler db download [species]` and updated with `coreprofiler db update [species]`. When a database is updated, update stored profiles with `coreprofiler platform profiles_update [args]`, and re-run `coreprofiler db get_num_alleles [args]`.
To limit redondant processes between two database updates, consider running `coreprofiler db makeblastdb [args]` and `coreprofiler db get_num_alleles [args]`, and providing their output file when needed.

Suggested (minimal) directory structure of required data and outputs:
```
cgMLST_data/
|
|-- schemes/
|   |-- scheme_files/
|   |    |-- kp/
|   |        |-- locus1.fasta
|   |        |-- locus2.fasta
|   |    |-- ecoli/
|   |-- update_logs/
|   |     |-- kp/
|   |       |-- YYYY-MM-DD1.log
|   |       |-- YYYY-MM-DD2.log
|   |     |-- ecoli/
|   |-- blastdb/
|   |     |-- kp/
|   |         |-- [blastdb files]
|   |     |-- ecoli/
|   |-- num_alleles_per_locus/
|   |     |-- kp.tsv
|   |     |-- ecoli.tsv
|
|-- genomes_stored/
    |-- assemblies/
    |    |-- kp/
    |        |-- genome1.fasta
    |        |-- genome2.fasta
    |    |-- ecoli/
    |-- profiles/
    |    |-- kp/
    |        |-- genome1_profile.tsv
    |        |-- genome2_profile.tsv
    |    |-- ecoli/
    |--- profiles_w_tmp_alleles
    |    |-- kp.pickle
    |    |-- ecoli.pickle
```

### Platform parameters
- `-hash`: whether new alleles should be returned with a hash. Else, TEMPORARY numerical value (default).
- `--profiles_w_tmp_alleles`: pickle file containing list of profiles with temporary alleles
- `--num_alleles_per_locus`: tsv file containing the number of alleles per locus of a given scheme [incompatible with `-hash`]

### Downloading and updating schemes
Schemes must be downloaded from reference platforms. The command `coreprofiler db download -s [species] -o [output_dir]` allows to download the scheme of a given species, using its reference platforms API. The list of available species can be found with: `coreprofiler db download av`.

To update a scheme, run `coreprofiler db update -s [species] -d [local_scheme_path] -o [update_log_path]`. This code reads the last local update of the species scheme from `data/scheme_api.json`, and replaces all locus files if the last remote locus update is newer than the last local database update. 

Available species schemes and their API requests URL are stored in `data/scheme_api.json`.

### Handling temporary alleles
When a new allele is detected, the sequence is extracted and the allele identifier is a MD5-hash of the sequence. This way, one new allele described in parallelized runs is given the same id in output profiles (and from one batch run to another). On a platform that aims to store query genomes and update cgMLST profiles, the `-locus_tmp_alleles_file` parameter should be provided, together with the `-hash` argument, to write the profiles containing temporary new alleles in a json file.

When a species scheme is updated, one should run `coreprofiler platform update_profiles -u [update.log] -p [profiles_w_tmp_alleles] -s [scheme_dir/]` to update the profiles with temporary alleles. This will parse the `update.log`, get the loci that have been updated, select the stored genomes that have temporary alleles for those loci, and run allele calling of those genomes against the updated loci. If exact matches are found, the program updates the genome's profiles and removes the "genome x locus" combination from `profiles_w_tmp_alleles`. Please note that, in order to keep update runs minimal, the profiles should be updated at each update of a species database. If the profiles are several updates behind the database, consider running several updates with the different logs (order does not matter).


Omitting the `-hash` argument (default, to simplify basic usage) allows to put out profiles with numerical values instead of a hash as identifier of temporary alleles. These profiles are meant for end-user and downstream anayses, as the latter often only take as input numerical values. That is done by replacing the hash identifier by "[number of alleles of this locus] + 1". Is it very important to note that these profiles are not stable: if the reference platforms describes a new allele different from the one described by CoreProfiler, and also calls it "#alleles + 1", the profiles might diverge. To compare genomes for downstream analyses, those need to be converted to numerical within the same database update. Run `coreprofiler platform hash_to_numerical -i [genomes list] -o [output dir]` to do so. [TODO: needs `num_alleles_per_locus` or the scheme directory!]

**Files description**
Minimal example of `profiles_w_tmp_alleles/species.pickle`:
```
{
    "strainX:
        {
            "genome": genome_path,
            "profile": profile_path,
            "tmp_loci":
                ["locus1",
                 "locus2"]
        }
}
```

Minimal example of `update.log`:
```
{
    "date": "YYYY.MM.DD"
    "loci_updated":
        [
         "locus1a"
         "locus2a"
        ]
}
```

### Optimizing performance with precomputed files
Two files can be provided to avoid repeating expensive processes:
- `--blast_db_path`: CoreProfiler runs BLAST functions on the query genomes vs. a BLAST databaes of the species scheme. When the user does not provide a BLAST database, one is created. Building such BLAST database takes some time, and does not need to be repeated unless the scheme has been updated. So, between two updates of a species scheme, consider providing a BLAST database as input. Create one running `coreprofiler db makeblastdb -s [scheme_path] -n [blastdb_name] -p [blastdb_path]`. `'blastdb_name'` is used to name output files, so provide a name compatible with file naming. To provide it as input, add the argument `-db [blastdb_path/blastdb_name.fasta]` to `coreprofiler allele_calling`.
- `--num_alleles_per_locus`: `coreprofiler platform hash_to_numerical` requires the number of alleles of each locus of a scheme. Processing this information is expensive as it needs to read and parse all locus files. Between two updates of the scheme database, consider storing the values with the command `coreprofiler db get_num_alleles -s [scheme_dir] -o [output_file.tsv]`. Re-run it everytime the database is updated.

## Testing
### Unittary testing
Unittary tests are written with pytest and can be run with the simple `pytest` command. To test the API and HTML requests, run `pytest -m requests`.

### Accuracy verification
CoreProfiler is being thoroughly tested against reference platforms.
- Autotag: tested by downloading public genomes and their profiles from the platform, running CoreProfiler cgMLST on the genomes and comparing the alleles called. [add details]
- Scannew: tested by manually removing alleles from the scheme database and verifying that these are described by CoreProfler "scannew" function.

# References
- Altschul, S. F., Gish, W., Miller, W., Myers, E. W., & Lipman, D. J. (1990). Basic local alignment search tool. *Journal of molecular biology*, 215(3), 403-410.
