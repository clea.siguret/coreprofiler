import argparse
import sys
import time
from contextlib import contextmanager
from pathlib import Path

from coreprofiler.__init__ import __version__


def parse_args() -> argparse.Namespace:  # pragma: no cover​
    """
    Parse command-line arguments.

    :return: An object containing the parsed command-line arguments.
    :rtype: argparse.Namespace
    :raises argparse.ArgumentError: If there is an error parsing
        the command-line arguments.
    """

    parser = argparse.ArgumentParser(description="cgMLST tool based on BLAST.")

    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version=f"CoreProfiler {__version__}",
    )

    subparsers = parser.add_subparsers(
        dest="mode", help="['allele_calling', 'db', 'platform']"
    )

    ###
    # Allele calling
    ###
    allele_calling_parser = subparsers.add_parser(
        "allele_calling", description="Allele calling specific arguments"
    )

    # General parameters.
    allele_calling_parser.add_argument(
        "-q",
        "--query",
        type=Path,
        nargs="+",
        required=True,
        help="Path(s) to query fasta file(s)",
    )
    allele_calling_parser.add_argument(
        "-sf", "--scheme_directory", required=True, type=Path, help="..."
    )
    allele_calling_parser.add_argument(
        "-out", required=True, type=Path, help="Path to output file"
    )
    allele_calling_parser.add_argument(
        "-db",
        "--blast_db_path",
        required=False,
        type=Path,
        help="Path to the BLAST database",
    )
    allele_calling_parser.add_argument(
        "-v", "--verbose", action="store_true", help="Verbose mode"
    )
    allele_calling_parser.add_argument(
        "-n", "--num_threads", type=int, default=4, help="Number of threads"
    )
    # Parameters for the autotag
    allele_calling_parser.add_argument(
        "--autotag_word_size",
        type=int,
        default=31,
        help="World size for Autotag BLASTn",
    )
    # Parameters for the detection of new alleles.
    allele_calling_parser.add_argument(
        "-cds", action="store_true", help="Extract new alleles within CDS"
    )
    allele_calling_parser.add_argument(
        "-d",
        "--detailed",
        action="store_true",
        help="Return further information on incomplete alleles",
    )
    allele_calling_parser.add_argument(
        "--min_id_new_allele",
        type=int,
        default=90,
        help="Minimum identity perc to consider new alleles.",
    )
    allele_calling_parser.add_argument(
        "--min_cov_new_allele",
        type=int,
        default=90,
        help="Minimum coverage perc to consider new alleles.",
    )
    allele_calling_parser.add_argument(
        "--min_cov_incomplete",
        type=int,
        default=70,
        help="Minimum coverage perc to consider an allele incomplete \
            (if --detailed option).",
    )
    # Parameters for integration on platform
    allele_calling_parser.add_argument(
        "--hash",
        action="store_true",
        help="Whether new alleles should be returned with a hash. \
            Else, TEMPORARY numerical value.",
    )
    allele_calling_parser.add_argument(
        "--profiles_w_tmp_alleles",
        required=False,
        type=Path,
        help="A Python pickle containing info about files with \
            temporary alleles.",
    )
    allele_calling_parser.add_argument(
        "--num_alleles_per_locus",
        required=False,
        type=Path,
        help="Tsv file containing the number of alleles per locus \
            of a given scheme.",
    )

    ###
    # Database
    ###
    db_parser = subparsers.add_parser(
        "db", description="Database handling specific arguments"
    )
    db_subparsers = db_parser.add_subparsers(
        dest="function", help="Functions of database mode"
    )
    # download
    download_parser = db_subparsers.add_parser(
        "download", help="Database download fonction"
    )
    download_parser.add_argument(
        "-s",
        "--species",
        required=True,
        type=str,
        help="Path to store the database.",
    )
    download_parser.add_argument(
        "-o",
        "--output_dir",
        required=False,
        type=Path,
        help="Path to store the database.",
    )

    # update_scheme
    update_scheme_parser = db_subparsers.add_parser(
        "update_scheme", help="Locally update a species scheme"
    )
    update_scheme_parser.add_argument(
        "-s",
        "--species",
        required=True,
        type=str,
        help="Species to consider",
    )
    update_scheme_parser.add_argument(
        "-d",
        "--local_scheme_path",
        required=True,
        type=Path,
        help="Local path to scheme directory",
    )
    update_scheme_parser.add_argument(
        "-o",
        "--update_log_path",
        required=True,
        type=Path,
        help="Path of the directory to write output logs",
    )

    # makeblastdb
    makeblastdb_parser = db_subparsers.add_parser(
        "makeblastdb", help="Run BLAST makeblasdtdb function"
    )
    makeblastdb_parser.add_argument(
        "-s",
        "--scheme_path",
        required=True,
        type=Path,
        help="Path to directory containing allele files",
    )
    makeblastdb_parser.add_argument(
        "-n",
        "--db_name",
        required=True,
        type=Path,
        help="BLAST db name",
    )
    makeblastdb_parser.add_argument(
        "-p",
        "--db_path",
        required=True,
        type=Path,
        help="Path to write the database",
    )
    # get_num_alleles
    get_num_alleles_parser = db_subparsers.add_parser(
        "get_num_alleles", help="Get number of alleles per locus"
    )
    get_num_alleles_parser.add_argument(
        "-s",
        "--scheme_dir",
        required=True,
        type=Path,
        help="Path to scheme directory",
    )
    get_num_alleles_parser.add_argument(
        "-o", "--output", required=True, type=Path, help="Output file"
    )

    ###
    # Platform
    ###
    platform_parser = subparsers.add_parser(
        "platform", description="Platform specific arguments."
    )

    platform_subparsers = platform_parser.add_subparsers(
        dest="function", help="Functions of platform mode."
    )
    # update_profiles
    update_profiles_parser = platform_subparsers.add_parser(
        "update_profiles", help="Update schemes with new alleles."
    )
    update_profiles_parser.add_argument(
        "-p",
        "--profiles_w_temp_alleles",
        required=True,
        type=Path,
        help="File with the profiles having temp alleles",
    )
    update_profiles_parser.add_argument(
        "-u",
        "--update_log",
        required=True,
        type=Path,
        help="Path to the output log",
    )
    update_profiles_parser.add_argument(
        "-s",
        "--scheme_dir",
        required=True,
        type=Path,
        help="Directory with scheme files",
    )
    update_profiles_parser.add_argument(
        "-v", "--verbose", action="store_true", help="Verbose mode"
    )
    update_profiles_parser.add_argument(
        "-n", "--num_threads", type=int, default=4, help="Number of threads"
    )

    # Display help if script called without any argument
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()

    # If only mode provided, print help
    if args.mode != "allele_calling" and args.function is None:
        mode_parser = subparsers.choices[args.mode]
        mode_parser.print_help()
        sys.exit(1)

    return args


@contextmanager  # pragma: no cover​
def timeit(message):
    start_time = time.monotonic()
    try:
        yield
    finally:
        end_time = time.monotonic()
        print(f"\n{message}: {end_time - start_time}s.\n")
