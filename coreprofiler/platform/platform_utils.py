import json
import pandas as pd
import pickle
import tempfile

from pathlib import Path

from coreprofiler.cgmlst import autotag, blast_functions


class ProfileConflictError(Exception):
    """
    Custom exception for profile conflicts.
    """

    pass


def update_profile(
    genome_path: Path,
    profile_path: Path,
    loci_updated: list[str],
    scheme_files_dir: Path,
    verbose: bool = False,
    num_threads: int = 4,
) -> list[str]:
    """Run autotag on updated loci and replace the profile if exact
    matches are found.
    """
    # Build a db with updated loci
    # Note: all alleles of updated loci are considered to avoid file parsing
    with tempfile.TemporaryDirectory() as tmp_dir:
        loci_updated_db = Path(tmp_dir) / "loci_updated_files"
        blast_functions.create_blast_db(
            scheme_path=scheme_files_dir,
            blast_db_name="loci_updated",
            blast_db_path=loci_updated_db,
            verbose=verbose,
            loci=loci_updated,
        )

        # Run autotag to detect new exact matches
        exact_matches_dict = autotag.autotag(
            query=genome_path,
            blast_db_path=loci_updated_db / "loci_updated.fasta",
            num_threads=num_threads,
            autotag_word_size=31,
            verbose=verbose,
        )

    # Replace profile file
    if exact_matches_dict:
        with open(profile_path, "r") as pp:
            profile = pd.read_csv(pp, sep="\t", index_col=0)

        # Verify that only alleles to update are temporary or missing alleles
        for locus in exact_matches_dict.keys():
            if str(profile.loc[str(genome_path), f"{locus}"]).isdigit():
                raise ProfileConflictError(
                    "Trying to update an allele that is not a hash."
                )
            # Replace the value
            profile.loc[str(genome_path), f"{locus}"] = exact_matches_dict[
                locus
            ]

        # Overwrite profile file
        with open(profile_path, "w") as pp:
            profile.to_csv(pp, sep="\t", index=True)

        # list of updated loci
        return list(exact_matches_dict.keys())


def update_profiles_from_logs(
    update_log_file: Path,
    profiles_w_temp_alleles_file: Path,
    scheme_dir: Path,
    verbose: bool = False,
    num_threads: int = 4,
):
    """Look for new exact matches on profiles whom
    temporary alleles have been updated.

    Overwrite profiles and update profiles_w_temp_alleles pickle.
    """
    with open(update_log_file, "r") as ulf:
        # Loci updated in the database
        loci_updated_db = json.load(ulf)["loci_updated"]

    with open(profiles_w_temp_alleles_file, "rb") as ptaf:
        profiles_w_temp_alleles = pickle.load(ptaf)

    loci_updated_in_profiles = {}

    for strain in profiles_w_temp_alleles.keys():
        # Get intersection
        # of loci updated in the database
        # and loci with a temporary id in the strain's profile
        tmp_loci_updated = list(
            set(profiles_w_temp_alleles[strain]["tmp_loci"])
            & set(loci_updated_db)
        )
        if tmp_loci_updated:
            # loci_exact_match stores the exact matches, i.e.,
            # the loci that have been updated on the profile.
            loci_exact_match = update_profile(
                genome_path=profiles_w_temp_alleles[strain]["genome"],
                profile_path=profiles_w_temp_alleles[strain]["profile"],
                loci_updated=tmp_loci_updated,
                scheme_files_dir=scheme_dir,
                verbose=verbose,
                num_threads=num_threads,
            )
            if loci_exact_match:
                loci_updated_in_profiles[strain] = loci_exact_match

    # Update profiles_w_temp_alleles
    strain_w_no_tmp_locus = []
    if loci_updated_in_profiles:
        for strain in profiles_w_temp_alleles.keys():
            tmp_loci = profiles_w_temp_alleles[strain]["tmp_loci"]
            # remove loci that have been updated
            tmp_loci_up = [
                locus
                for locus in tmp_loci
                if locus not in loci_updated_in_profiles[strain]
            ]
            # If no temporary locus remains, delete the strain
            if tmp_loci_up:
                profiles_w_temp_alleles[strain]["tmp_loci"] = tmp_loci_up
            else:
                strain_w_no_tmp_locus.append(strain)

        for strain in strain_w_no_tmp_locus:
            profiles_w_temp_alleles.pop(strain)

        with open(profiles_w_temp_alleles_file, "wb") as ptaf:
            pickle.dump(profiles_w_temp_alleles, ptaf)
