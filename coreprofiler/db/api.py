import requests
import json
import pandas as pd
from datetime import datetime
from pathlib import Path

###############################
# --- schemes from BIGSdb --- #
###############################


def fetch_remote_alleles(locus_url: str):
    """Get alleles file of a locus through BIGSdb's API.

    :param locus: locus name
    :type locus: string
    :param species_url: API endpoint URL for accessing the alleles list
    :type species_url: string
    :return: alleles list
    :rtype: string
    """
    response_seqs = requests.get(locus_url + "/alleles_fasta")
    response_info = requests.get(locus_url + "/alleles?return_all=1")
    if response_seqs.status_code == 200 and response_info.status_code == 200:
        alleles_info = requests.get(locus_url + "/alleles?return_all=1").json()
        last_update_remote_locus = datetime.strptime(
            alleles_info["last_updated"], "%Y-%m-%d"
        ).date()
        return {
            "seq": response_seqs.content.decode("utf-8"),
            "last_update": last_update_remote_locus,
        }
    else:
        return f"Failed to fetch data from url: {locus_url}"


def download_scheme(
    scheme_api_file: Path, species: str, scheme_local_path: Path = None
):
    """Download a cgMLST scheme from BIGSdb's API.

    :param scheme_api_file: JSON file containg API information
    :type scheme_api_file: pathlib.Path
    :param species: species to consider
    :type species: string
    :param scheme_local_path: Path of the directory where to write
        the scheme files
    :type scheme_local_path: pathlib.Path
    """

    with open(scheme_api_file, "r") as file:
        data = json.load(file)
        df = pd.DataFrame.from_dict(
            data, orient="index", columns=["description", "url"]
        )
        species_av = df.index.tolist()

        if species == "av":
            print(species_av)

        elif species in species_av:
            scheme_url = df.loc[species, "url"]
            loci_url = requests.get(scheme_url).json()["loci"]
            scheme_local_path.mkdir(parents=True, exist_ok=True)
            for locus_url in loci_url:
                locus = Path(locus_url).name
                alleles_fasta = fetch_remote_alleles(locus_url)["seq"]
                with open(scheme_local_path / f"{locus}.fasta", "w+") as f:
                    f.write(alleles_fasta)
        else:
            print(
                "Invalid species selection. Pleaes choose one species "
                f"from the following: {species_av}"
            )


def update_scheme(
    species: str,
    scheme_file: Path,
    scheme_local_path: Path,
    log_dir_path: Path,
):
    """Locally replace scheme files that have been updated on BIGSdb.
    Write update log.

    :param species: species to uptdate scheme
    :type species: str
    :param scheme_local_path: local path of the scheme directory
    :type scheme_local_path:  pathlib.Path
    :param log_dir_path: Path of the directory to write output files
    :type log_dir_path: pathlib.Path
    """

    # Get last local scheme update date
    with open(scheme_file, "r") as sf:
        scheme_info = json.load(sf)
        last_local_update = datetime.strptime(
            scheme_info[species]["last_local_update"], "%Y-%m-%d"
        ).date()

    locus_file = {file.stem: file for file in scheme_local_path.iterdir()}
    loci_url = requests.get(scheme_info[species]["url"]).json()["loci"]
    locus_url = {url.split("/")[-1]: url for url in loci_url}
    updated_loci = []

    # Replace updated locus files
    for locus, file in locus_file.items():
        locus_info = fetch_remote_alleles(locus_url[locus])
        if locus_info["last_update"] > last_local_update:
            updated_loci.append(locus)
            with open(file, "w+") as f:
                f.write(locus_info["seq"])

    today = datetime.today().strftime("%Y-%m-%d")
    # Write the update log
    log_dir_path.mkdir(parents=True, exist_ok=True)
    log_file = log_dir_path / f"{today}.log"
    with open(log_file, "w+") as logf:
        logf.write(json.dumps({today: updated_loci}))

    # Modify 'last_local_update' in data/scheme_api.json
    scheme_info[species]["last_local_update"] = today
    with open(scheme_file, "w") as sf:
        sf.write(json.dumps(scheme_info, indent=4))
