from urllib.request import urlopen
from urllib.error import HTTPError
import urllib
import base64
import json

"""
Code snippets from:
https://enterobase.readthedocs.io/en/latest/api/api-getting-started.html
"""


API_TOKEN = "YOUR_TOKEN_HERE"


def __create_request(request_str):
    base64string = base64.b64encode("{0}: ".format(API_TOKEN).encode("utf-8"))
    headers = {"Authorization": "Basic {0}".format(base64string.decode())}
    request = urllib.request.Request(request_str, None, headers)
    return request


address = 'https://enterobase.warwick.ac.uk/' \
    'api/v2.0/senterica/' \
    'straindata?assembly_status=Assembled&limit=1'

try:
    response = urlopen(__create_request(address))
    data = json.load(response)
    print(json.dumps(data, sort_keys=True, indent=4, separators=(",", ": ")))

except HTTPError as Response_error:
    print(
        "%d %s. <%s>\n Reason: %s"
        % (
            Response_error.code,
            Response_error.reason,
            Response_error.geturl(),
            Response_error.read(),
        )
    )
