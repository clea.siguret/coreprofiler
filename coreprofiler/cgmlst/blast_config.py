import re
from dataclasses import dataclass
from typing import Optional

from pathlib import Path


@dataclass
class BlastConfig:
    blast_type: str
    query_path: Path
    task: str
    num_threads: int
    ungapped: bool
    outfmt: str
    word_size: int
    max_target_seqs: Optional[int] = None
    perc_identity: Optional[int] = None
    db_path: Optional[Path] = None
    subject: Optional[str] = None
    evalue: Optional[str] = None

    def __post_init__(self):
        if self.blast_type not in ["blastn", "tblastn"]:
            raise ValueError("Invalid BLAST type")
        if not self.query_path.is_file():
            raise FileNotFoundError("Invalid query file path")
        if self.db_path is None and self.subject is None:
            raise ValueError("Either db_path or subject must be specified")
        if self.db_path is not None and self.subject is not None:
            raise ValueError("Only one of db_path or subject can be specified")
        if self.db_path is not None and not self.db_path.is_file():
            raise FileNotFoundError("Invalid BLAST database file path")
        if self.task not in ["blastn", "megablast"]:
            raise ValueError("Invalid BLAST task")
        if not isinstance(self.num_threads, int) or self.num_threads < 1:
            raise ValueError("Invalid number of threads")
        if self.word_size and (
            not isinstance(self.word_size, int) or self.word_size <= 5
        ):
            raise ValueError("Invalid word size")
        if self.perc_identity and (
            not isinstance(self.perc_identity, int)
            or self.perc_identity < 0
            or self.perc_identity > 100
        ):
            raise ValueError("Invalid percent identity")
        if self.max_target_seqs and (
            not isinstance(self.max_target_seqs, int)
            or self.max_target_seqs < 1
        ):
            raise ValueError("Invalid maximum number of target sequences")
        if not isinstance(self.ungapped, bool):
            raise ValueError("Invalid ungapped value")
        if not re.match(r"^6\s.+$", " ".join(self.outfmt)):
            raise ValueError("Invalid outfmt value")

    def get_blast_cmd(self):
        blast_cmd = [
            self.blast_type,
            "-query",
            self.query_path,
            "-task",
            self.task,
            "-num_threads",
            str(self.num_threads),
            "-outfmt",
            " ".join(self.outfmt),
        ]
        if self.max_target_seqs:
            blast_cmd += ["-max_target_seqs", str(self.max_target_seqs)]
        if self.evalue:
            blast_cmd += ["-evalue", self.evalue]
        if self.ungapped:
            blast_cmd.append("-ungapped")
        if self.perc_identity:
            blast_cmd += ["-perc_identity", str(self.perc_identity)]
        if self.blast_type == "blastn":  # TODO: case of tblastn
            blast_cmd += [
                "-dust",
                "no",
                "-word_size",
                str(self.word_size),
            ]
        if self.db_path:
            blast_cmd += ["-db", self.db_path]
        elif self.subject:
            blast_cmd += ["-subject", self.subject]

        return blast_cmd
