from typing import Dict

from coreprofiler.cgmlst import blast_config, blast_functions

from pathlib import Path

AUTOTAG_OUTFMT = (
    "6",
    "evalue",
    "bitscore",
    "pident",
    "sseqid",
    "slen",
    "length",
    "nident",
    "qseqid",
    "qstart",
    "qend",
    "sstrand",
)


def autotag(
    query: Path,
    blast_db_path: Path,
    autotag_word_size: int,
    num_threads: int,
    verbose: bool,
) -> Dict[str, str]:
    """
    Runs a BLAST command and parses for exact hits (id = 100%
        and coverage = 100%).

    :param query: The query path
    :param blast_db: The BLAST database path.
    :param autotag_word_size: Word size for MEGABlast.
        Use len(shortest allele) -1.
    :param num_threads: The number of threads to run BLAST.
    :return: A dictionary with locus as the key and allele as the value.
    """
    if verbose:
        print("--- Step 1: autotag ---")

    autotag_blast_config = blast_config.BlastConfig(
        blast_type="blastn",
        query_path=query,
        db_path=blast_db_path,
        task="megablast",
        word_size=autotag_word_size,
        num_threads=num_threads,
        perc_identity=100,
        max_target_seqs=int(1e7),
        ungapped=True,
        outfmt=AUTOTAG_OUTFMT,
    )

    autotag_blast = blast_functions.run_blast(autotag_blast_config)

    autotag_blast["locus"] = autotag_blast["sseqid"].str.extract(r"(.+)_\d+$")[
        0
    ]
    autotag_blast["allele"] = autotag_blast["sseqid"].str.extract(r"_(\d+)$")[
        0
    ]

    # Define subject_coverage
    autotag_blast["subject_coverage"] = (
        autotag_blast["length"] / autotag_blast["slen"]
    ) * 100

    exact_matches_df = autotag_blast.loc[
        autotag_blast["subject_coverage"] == 100
    ]

    # Keep hit with highest bitscore for each locus
    # Note: an allele may be contained into another
    exact_matches = (
        exact_matches_df.loc[
            exact_matches_df.groupby("locus")["bitscore"].idxmax()
        ]
        .set_index("locus")["allele"]
        .to_dict()
    )

    if verbose:
        print("\nEnd of autotag.\n")

    return exact_matches
