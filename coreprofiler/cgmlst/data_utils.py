import re
import pandas as pd


def hash_to_numerical(
    allele_calling_hash: pd.DataFrame,
    locus_temp_alleles: dict,
    number_alleles: dict,
) -> pd.DataFrame:
    """Converts MD5-hash of new alleles to numerical values
    (# official alleles + 1) for downstream cgMLST analyses.

    :param allele_calling_hash: The allele calling df with MD5-hash.
    :param locus_temp_alleles: By locus, the list of profiles containg alleles
        described by the program.
    :param number_alleles: The number of official alleles for each locus.
    :return: New df with only with numerical values
    """
    allele_calling_df = allele_calling_hash.copy()
    number_alleles_t0 = number_alleles
    hash_encountered = {}

    for locus in locus_temp_alleles.keys():
        for isolate in locus_temp_alleles[locus]:
            if isolate in allele_calling_df.index:
                allele = allele_calling_df.loc[isolate, locus]
                # Verify that the string after the last '_'
                # is a MD5-hash
                if not re.match(r".*_([a-fA-F\d]{32})$", str(allele)):
                    raise ValueError(
                        "The hash_to_numerical conversion tries to apply \
                        on a non-hash value."
                    )
                elif allele in hash_encountered.keys():
                    allele_calling_df.loc[isolate, locus] = hash_encountered[
                        allele
                    ]
                else:
                    allele_calling_df.loc[isolate, locus] = (
                        int(number_alleles_t0[locus]) + 1
                    )
                    hash_encountered[allele] = (
                        int(number_alleles_t0[locus]) + 1
                    )
                    number_alleles_t0[locus] = (
                        int(number_alleles_t0[locus]) + 1
                    )
    return allele_calling_df
