from pathlib import Path

from Bio import SeqIO

from coreprofiler.cgmlst.constants import FASTA_EXTENSIONS


class ContigNotFound(Exception):
    pass


def get_sequence_from_fasta_file(filename: Path, contig_id: str) -> str:
    """Extract the sequence of an allele from the fasta file
        containing the allele.

    :param filename: The path to the fasta file
    :type filename: pathlib.Path
    :param contig_id: Contig identifier
    :type contig_id: str
    :raises ContigNotFound: If the allele could not be found for the locus.
    :return: The complete sequence
    :rtype: str
    """
    with open(filename, "r") as fasta_file:
        for record in SeqIO.parse(fasta_file, "fasta"):
            if record.id == contig_id:
                return str(record.seq)
    raise ContigNotFound(f"Contig {contig_id} not found in {filename}.")


def get_locus_file(locus: str, scheme_path: Path) -> Path:
    """Return the path of the locus file in the scheme directory.
    Looks through the different extensions.

    :param locus: Locus name.
    :type locus: str
    :param scheme_path: The path to the directory containing the locus files.
    :type scheme_path: pathlib.Path
    :return: The file path.
    :rtype: pathlib.Path
    :raises FileNotFoundError: If no file could be found
        with a fasta extension.
    """
    for ext in FASTA_EXTENSIONS:
        file_path = scheme_path / (locus + ext)
        if file_path.is_file():
            return file_path
    raise FileNotFoundError(
        f"No file found for locus {locus} with a \
            fasta extension in directory {scheme_path}."
    )


def extract_loci_names(directory_path: Path) -> list[str]:
    """
    Returns a list of the names of all loci files in a directory.

    :param directory_path: The path to the directory containing the loci files.
    :type directory_path: pathlib.Path
    :return: A list of the names of all loci files in the directory,
        without their file extensions.
    :rtype: list
    :raises FileNotFoundError: If the input directory does not exist
        or does not contain any loci files.
    """
    if not directory_path.is_dir():
        raise FileNotFoundError(f"Directory {directory_path} not found.")

    loci_files = []

    for extension in FASTA_EXTENSIONS:
        loci_files.extend(directory_path.glob(f"**/*{extension}"))

    if not loci_files:
        raise FileNotFoundError(
            "No file with extensions .fna, .fasta, .fas, .tfa \
            found in the directory."
        )
    return [file.stem for file in loci_files]


def get_num_alleles(scheme_directory: Path):
    """Returns the number of alleles per locus of a scheme.

    :param scheme_directory: Directory containing fasta files,
        one per locus, with name `locus` and a fasta extension.
    :type scheme_directory: Path
    :return: Number of alleles per locus.
    :rtype: dict[str: int]
    """
    num_alleles = {}
    for file_path in scheme_directory.glob("*"):
        if any(file_path.name.endswith(ext) for ext in FASTA_EXTENSIONS):
            locus_name = file_path.stem
            num_sequences = sum(1 for _ in SeqIO.parse(file_path, "fasta"))
            num_alleles[locus_name] = num_sequences
    return num_alleles
