import hashlib
import tempfile
from pathlib import Path
from typing import Dict

import pandas as pd

from coreprofiler.cgmlst import blast_functions
from coreprofiler.cgmlst.blast_config import BlastConfig

SCANNEW_OUTFMT = (
    "6",
    "evalue",
    "bitscore",
    "pident",
    "sseqid",
    "slen",
    "length",
    "nident",
    "qseqid",
    "qstart",
    "qend",
    "sstrand",
    "qseq",
)


def scannew(
    query: str,
    no_exact_match_loci: list[str],
    scheme_directory: Path,
    num_threads: int,
    cds: bool,
    verbose: bool,
    detailed: bool,
    min_id: float,
    min_cov_new_allele: float,
    min_cov_incomplete: float,
) -> (pd.DataFrame, Dict[str, str]):
    """
    Scan a set of missing loci against a local database and return the results.

    :param query: The query sequence in FASTA format.
    :param missing_loci: A list of missing loci names.
    :param scannew_blast_min_cov: The minimum coverage required for a
        BLAST match to be considered.
    :param local_db_path: The path to the local database directory.
        Defaults to None.
    :param cds: Whether the query sequence is a coding sequence.
        Defaults to False. [UNDER DEVELOPMENT]
    :param num_threads: The number of threads to use for BLAST.
    :param verbose: Whether to print verbose output.
    :return: A tuple containing the exact matches to the local database,
        the new alleles found, the new allele sequences, the incomplete
        alleles found, and the absent loci.
    :raises Exception: If the missing loci could not be found in the scheme
        files directory.
    """
    if verbose:
        print("--- Step 2: scanning for new alleles ---")

    scannew_dict = {}

    # Create a BLAST database in a temporary directory
    with tempfile.TemporaryDirectory() as tmp_dir:
        joined_fasta_files = Path(tmp_dir) / "joined_fasta_files_tmp.fasta"
        blast_functions.create_blast_db(
            scheme_directory,
            blast_db_name=joined_fasta_files.name,
            blast_db_path=Path(tmp_dir),
            verbose=verbose,
            loci=no_exact_match_loci,
        )

        # Run BLAST
        scannew_blast_config = BlastConfig(
            blast_type="blastn",
            query_path=query,
            db_path=joined_fasta_files,
            task="blastn",
            word_size=31,
            num_threads=num_threads,
            outfmt=SCANNEW_OUTFMT,
            perc_identity=90,
            max_target_seqs=int(1e6),
            ungapped=False,
        )

        scannew_blast = blast_functions.run_blast(scannew_blast_config)

        scannew_blast["locus"] = scannew_blast["sseqid"].str.extract(
            r"(.+)_\d+$"
        )[0]
        scannew_blast["allele"] = scannew_blast["sseqid"].str.extract(
            r"_(\d+)$"
        )[0]

    # Keep only the best match
    best_match_per_locus = scannew_blast.groupby("locus").first().reset_index()

    # Define subject_coverage
    best_match_per_locus["subject_coverage"] = (
        best_match_per_locus["length"] / best_match_per_locus["slen"]
    ) * 100

    # Keep only matches that fit criteria
    scannew_df = best_match_per_locus[
        (best_match_per_locus["pident"] >= min_id)
        & (best_match_per_locus["subject_coverage"] >= min_cov_new_allele)
    ]

    # Get new alleles
    new_alleles = scannew_df.loc[
        (scannew_df["pident"] > min_id)
        & (scannew_df["subject_coverage"] > min_cov_new_allele)
    ].copy()

    new_alleles["is_cds"] = "0"
    new_alleles.rename(columns={"qseq": "seq"}, inplace=True)

    if not new_alleles.empty:
        # MD5-hash the sequences
        new_alleles["md5"] = new_alleles.apply(
            lambda row: row["locus"]
            + "_"
            + hashlib.md5(row["seq"].encode()).hexdigest(),
            axis=1,
        )
        scannew_dict.update(
            new_alleles.set_index("locus")["md5"].to_dict()
        )

    # Get more information about the other best matches
    # Conditions must be incompatible with new_alleles
    # Be careful that values don't overlap
    incomplete_list = []
    if detailed:
        incomplete_list = scannew_df.loc[
            (scannew_df["pident"] != 100)
            & (scannew_df["pident"] > min_id)
            & (scannew_df["subject_coverage"] < min_cov_new_allele)
            & (scannew_df["subject_coverage"] > min_cov_incomplete)
        ]["locus"].to_list()
        scannew_dict.update({locus: "N" for locus in incomplete_list})

    # Add symbol for missing loci
    missing_loci = [
        locus
        for locus in no_exact_match_loci
        if locus not in scannew_dict.keys()
    ]
    scannew_dict.update({locus: "-" for locus in missing_loci})

    if verbose:
        print("\n--- end of scannew ---\n")
        print(f"Number of new alleles described: {len(new_alleles)}.")
        if detailed:
            print(f"Number of incomplete loci: {len(incomplete_list)}.")
        print(f"Number of missing loci: {len(missing_loci)}.\n")

    assert len(new_alleles) + len(missing_loci) + len(incomplete_list) == len(
        no_exact_match_loci
    )

    return new_alleles, scannew_dict
