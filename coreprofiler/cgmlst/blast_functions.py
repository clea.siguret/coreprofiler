import subprocess
from io import StringIO
from pathlib import Path
from typing import Optional

import pandas as pd

from coreprofiler.cgmlst.blast_config import BlastConfig
from coreprofiler.cgmlst.constants import FASTA_EXTENSIONS


def run_blast(blast_config: BlastConfig):
    blast = subprocess.run(
        blast_config.get_blast_cmd(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=True,
    )

    blast_output_df = pd.read_csv(
        StringIO(blast.stdout),
        sep="\t",
        header=None,
        index_col=False,
        names=blast_config.outfmt[1:],
    )  # [1:] to remove the '6' from the outfmt string

    return blast_output_df


def create_blast_db(
    scheme_path: Path,
    blast_db_name: str,
    blast_db_path: Path,
    verbose: bool,
    loci: Optional[list] = None,
) -> None:
    """Create a BLAST database from the specified input files.

    :param scheme_path: Path to the directory containing alleles files.
    :param blast_db_name: The name wanted for the database.
    :param blast_db_path: The path to the output database file.
    :param verbose: Verbosity.
    :param loci (optional): List of loci to build the database.
        If not provided, all fasta files in directory.
    :return: None
    """

    loci_files = [
        file
        for file in scheme_path.iterdir()
        if file.suffix in FASTA_EXTENSIONS and (not loci or file.stem in loci)
    ]

    # Concatenate loci files
    # makeblastdb requires a single file as input containing
    # all the loci and alleles
    # in the format >locus_allele

    blast_db_fasta_file = (blast_db_path / blast_db_name).with_suffix(".fasta")

    blast_db_fasta_file.parent.mkdir(exist_ok=True, parents=True)
    with open(blast_db_fasta_file, "w+") as outfile:
        for locus_file in loci_files:
            with open(locus_file) as infile:
                for line in infile:
                    outfile.write(line)

    # Run makeblastdb command
    stdout_val = None if verbose else subprocess.DEVNULL
    subprocess.run(
        [
            "makeblastdb",
            "-in",
            str(blast_db_fasta_file),
            "-dbtype",
            "nucl",
            "-title",
            "tmp_blastdb",
        ],
        stdout=stdout_val,
        check=True,
    )
