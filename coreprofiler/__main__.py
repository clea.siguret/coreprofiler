import csv

from coreprofiler.utility import parse_args
from coreprofiler.cgmlst.allele_calling import allele_calling
from coreprofiler.cgmlst.blast_functions import create_blast_db
from coreprofiler.cgmlst.file_utils import get_num_alleles
from coreprofiler.db.api import download_scheme, update_scheme
from coreprofiler.platform.platform_utils import update_profiles_from_logs

from pathlib import Path

scheme_api_file = Path("data/scheme_api.json")


def main():
    args = parse_args()

    if args.mode == "allele_calling":
        allele_calling(args)

    elif args.mode == "db":
        if args.function == "download":
            download_scheme(
                scheme_api_file=scheme_api_file,
                species=args.species,
                scheme_local_path=args.output_dir,
            )
        elif args.function == "makeblastdb":
            create_blast_db(
                scheme_path=args.scheme_path,
                blast_db_name=args.db_name,
                blast_db_path=args.db_path,
                verbose=True,
            )
        elif args.function == "get_num_alleles":
            num_alleles = get_num_alleles(args.scheme_dir)
            with open(args.output, "w+") as of:
                writer = csv.writer(of, delimiter="\t")
                for key, value in num_alleles.items():
                    writer.writerow([key, value])

        elif args.function == "update":
            update_scheme(
                species=args.species,
                scheme_file=scheme_api_file,
                scheme_local_path=args.local_scheme_path,
                log_dir_path=args.update_log_path,
            )

    elif args.mode == "platform":
        if args.function == "update_profiles":
            update_profiles_from_logs(
                update_log_file=args.update_log,
                profiles_w_temp_alleles_file=args.profiles_w_temp_alleles,
                scheme_dir=args.scheme_dir,
                verbose=args.verbose,
                num_threads=args.num_threads,
            )


if __name__ == "__main__":
    main()
parse_args
